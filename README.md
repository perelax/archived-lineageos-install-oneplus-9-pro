# OnePlus 9 Pro LineageOS 19.1

## Notes
This information is presented as is.  Please understand the risks 
associated with modifying your phone. 

The LineageOS wiki no longer displays the install instructions for older ROM's, such as Android 12 LineageOS 19.1.
This repo is a reconstruction of install methods to include firmware updating.

## Firmware
If you would like to update your firmware, visit [OnePlus Smartphone Software Update](https://service.oneplus.com/global/search/search-detail?id=2096329&articleIndex=1) and download the latest firmware.

**READ NOTES BELOW BEFORE BEGINNING**

The firmware update requires the use of the MSM Tool and a Windows PC.  
For a video example:
[Qualcomm CrashDump Mode Fix On Any OnePlus Device Easy!](https://www.youtube.com/watch?v=Sg0FpJfdIE0&t=66s)
You will need to gather a couple items before using the MSM Tool.
 - Get the Microsoft account information for the Windows box you will 
 be using.  You will need the Microsoft Recovery Key.
 - Boot into bios and turn secure boot off.
 - Finish booting the PC and enter recovery key.
 - Open the command prompt as administrator and enter:
 ```
bcdedit /set testsigning on

```
 - Turn off real-time virus scanning.
 - Start the MSM Tool as shown in the video.  I chose O2 as my 
 selection on an LE2125 Global device.
 - You have to hold both volume buttons at the same time, and then plug 
 the device into USB.  When it says 'connected', select start.
 - When you see 'Downloading' you may release the volume buttons.
 - When your device begins to reboot you may disconnect it.
 - Open the command prompt and enter:
 ```
bcdedit /set testsigning off

```
 - Reboot the PC and enter bios.
 - Turn secure boot back on.
 - Re-enter recovery key after boot.
 - Turn virus scanning back on.
 
## Clone Repo
 - In terminal, enter:
```
git clone https://gitlab.com/perelax/archived-lineageos-install-oneplus-9-pro.git

```
Open terminal in cloned folder and unzip vendor-boot.zip, twrp.zip, recovery.zip.
 
**Note**
 
In order to enable OEM Unlocking, the after-boot-start-up-wizard must 
complete.  WIFI is necessary for the update, so be sure to 
set up the WIFI connection during the initial boot wizard.
The after-boot wizard appears as a blue banner in the notifications 
panel.

## OEM Unlock
If necessary, enable oem unlock in Developer Settings.  

- In terminel, enter:
 ```
adb reboot bootloader

```
You should see the Fastboot screen.
- In terminel, enter:
 ```
fastboot oem unlock

```
The device will reboot.
You must now re-navigate the initial-boot setup wizard, enable Developer 
Options and USB Debugging again.

## Sequence
Our next sequence of steps follows:
1. Boot into TWRP and install copy_partitions.
2. Flashing dtbo, vendor_boot and lineage_recovery img files
3. Booting into Lineage Recovery and side-loading the nightly image.

**Note**
If you need an archived LineageOS ROM, please visit:
[LineageOS Build Archive](https://lineage-archive.timschumi.net/)
 
## Boot TWRP / Install Copy Partitions
**Copy/Paste copy-partitions-20220613-signed.zip to sdcard** 

This can be done at least 2 ways:
 - In terminel, enter:
 ```
adb push copy-partitions-20220613-signed.zip /sdcard/

```
OR
 - Simply copy/paste file into storage.  This would be the folder that 
 lists other folders:

 ![Copy Here](https://gitlab.com/perelax/src_imgs/-/raw/main/Archive/lineageos_install_copy_here.png)
 
 
- In terminel, enter:
 ```
adb reboot bootloader

```
You should see the Fastboot screen.
- In terminel, enter:
 ```
fastboot boot twrp-3.7.0_11-0-lemonadep.img

```
In TWRP:
 - Select Wipe -> use default settings, deselect Data/Storage so that copy-partitions doesn't get removed.
 - Select Install -> Choose the copy_partitions file
 - Swipe to install
 - Select Reboot -> bootloader

You should see the Fastboot screen.
In terminal, enter:
```
fastboot flash dtbo dtbo.img  &&
fastboot flash vendor_boot vendor_boot.img &&
fastboot flash boot lineage-19.1-20221025-recovery-lemonadep.img &&
fastboot reboot recovery

```

In recovery, sideload the LineageOS ROM. I chose the December 27, 2022 
Nightly Build as my install.  Change the file name to your ROM file. 
```
adb sideload lineage-19.1-20221227-nightly-lemonadep-signed.zip

```
Example:

![Install](https://gitlab.com/perelax/src_imgs/-/raw/main/Archive/lineageos_install_1.png)

**NOTE** 
You may encounter an error on reboot stating that the system is 
corrupt.  Select factory reset and it should boot into LineageOS.


## Support
Suggestions and corrections are welcome to the repo.

  
Feel like saying thank you?  

[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png){width=120}](https://www.buymeacoffee.com/perelax)

## Authors
Perelax

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

See <https://www.gnu.org/licenses/> for a copy of the GNU General Public License.
